/**
 * This source code is Copyright 2015 by Sanrasern Chaihetphon.
 */

package stopwatch;
/**
 * To add each char to be a String.
 * @author Sanrasern Chaihethon
 *
 */
public class AppendToString implements Runnable {
	/** Declare character to append. */
    final char each = 'a';
    
    /** Declare counter that is number in loop . */
	private int counter;
	
	/**
	 * Constructor of AppendToString.
	 * @param counter that is number of each that add.
	 */
	public AppendToString(int counter){
		this.counter = counter;
	}

	/**
	 * To append char to String.
	 */
	public void run(){
		String sum = ""; 
		int k = 0;
		while(k++ < counter) {
			sum = sum + each;
		}
		System.out.println("final string length = " + sum.length());
	}
	
	/**
	 * Get information about this class.
	 * @return String as What the class does.
	 */
	public String toString(){
		return String.format("Append to String with count=%,d\n", counter);
	}
}
