/**
 * This source code is Copyright 2015 by Sanrasern Chaihetphon.
 */
package stopwatch;

/**
 * To sum double from array.
 * @author Sanrasern Chaihetphon
 *
 */
public class SumDouble implements Runnable{
	
	private int counter;
	private static int arraySize;
	private double[] values;
	double sum = 0.0;
	
	/**
	 * Constructor of SumDouble.
	 * @param counter that is number of each that add.
	 * @param arraySize as size of array.
	 */
	public SumDouble(int counter,int arraySize){
		this.arraySize = arraySize;
		this.counter = counter;
		this.values = new double [arraySize];
		for(int i=0; i<this.arraySize; i++) values[i] = new Double(i+1);
	}

	/**
	 * Get information about this class.
	 * @return String as What the class does.
	 */
	public String toString(){
		return String.format("Sum array of Double objects with count=%,d\n", counter);
	}
	
	/**
	 * To run for execute varies code.
	 */
	public void run(){
		
		
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= arraySize) i = 0;
			sum = sum + values[i];
		}
		System.out.println("sum = " + sum);
	}
}
