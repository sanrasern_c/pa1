/**
 * This source code is Copyright 2015 by Sanrasern Chaihetphon.
 */
package stopwatch;

import java.util.Scanner;

/**
 * To test stopwatch.
 * @author Sanrasern Chaihetphon
 *
 */
public class StopWatchTest {

	/**
	 * Main that test the stopwatch.
	 * @param arg that is argument in method.
	 */
	public static void main (String [] arg){
		Scanner scan = new Scanner (System.in);
		StopWatch timer = new StopWatch();
		System.out.println("Starting task");
		timer.start();

		System.out.printf("elapsesd = %.6f sec\n", timer.getElapsed());

		if(timer.isRunning())
			System.out.println("timer is running");
		else
			System.out.println("timer is stopped");

		timer.stop();
		System.out.printf("elapsed = %.6f sec\n", timer.getElapsed());
		if(timer.isRunning())
			System.out.println("timer is running");
		else 
			System.out.println("timer is stopped");
		System.out.printf("elapsed = %.3f sec\n", timer.getElapsed());


	}


}
