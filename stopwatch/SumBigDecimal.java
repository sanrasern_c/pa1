/**
 * This source code is Copyright 2015 by Sanrasern Chaihetphon.
 */
package stopwatch;

import java.math.BigDecimal;

/**
 * To sum double in the array.
 * @author Sanrasern Chaihetphon
 *
 */
public class SumBigDecimal implements Runnable{
	private int counter;
	private static int arraySize;
	private BigDecimal[] values;
	BigDecimal sum = new BigDecimal(0.0);

	/**
	 * Constructor and initial value of attribute.
	 * @param counter as loop round.
	 * @param arraySize as size of array.
	 */
	public SumBigDecimal(int counter, int arraySize){
		this.arraySize = arraySize;
		this.counter = counter;
		this.values = new BigDecimal[arraySize];
		for(int i=0; i<this.arraySize; i++) values[i] = new BigDecimal(i+1);
	}

	/**
	 * Get information about this class.
	 * @return String as What the class does.
	 */
	public String toString(){
		return String.format("Sum array of BigDecimal with count=%,d\n", counter);
	}
	
	/**
	 * To run for execute varies code.
	 */
	public void run(){
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= arraySize) i = 0;
			sum = sum.add( values[i] );
		}
		System.out.println("sum = " + sum);
	}
}
