/**
 * This source code is Copyright 2015 by Sanrasern Chaihetphon.
 */
package stopwatch;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Scanner;

/**
 * Execute the program.
 * @author Sanrasern Chaihetphon
 *
 */
public class Main {
	private int counter = 100000;
	static final int ARRAY_SIZE = 500000;

	/**
	 * Set the loop count used in tests.
	 * @param count is number of loop iterations
	 */
	public void setCounter(int count) { this.counter = count; }


	/**
	 * To run the program.
	 * @param args is an argument.
	 * @throws IOException throws the exception.
	 */
	public static void main(String[] args) throws IOException {

		Runnable [] arrayTasks = new Runnable[]{
				/** Declare the task's array. */
			new AppendToString(100000),
			new AppendToStringBuilder(100000),
			new SumDoublePrimitive(100000000),
			new SumDouble(100000000,50000),
			new SumBigDecimal(100000000,50000)

		};
		TaskTimer task = new TaskTimer();

		for(int i=0; i<arrayTasks.length; i++ ){
			task.measureAndPrint(arrayTasks[i]);
		}


	}
}