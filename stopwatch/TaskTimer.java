/**
 * This source code is Copyright 2015 by Sanrasern Chaihetphon.
 */
package stopwatch;

/**
 * To run the time.
 * @author Sanrasern Chaihetphon
 *
 */
public class TaskTimer {
	
	/**
	 * To run all methods.
	 * @param runnable that is each task.
	 */
	public void measureAndPrint(Runnable runnable) {
		StopWatch timer = new StopWatch();

		System.out.print(runnable.toString());

		timer.start();

		runnable.run();  
		timer.stop();
		
		System.out.printf("Elapsed time %.6f sec\n\n", timer.getElapsed());
	}
}
