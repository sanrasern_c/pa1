/**
 * This source code is Copyright 2015 by Sanrasern Chaihetphon.
 */
package stopwatch;

/**
 * To add each char to be a String.
 * @author Sanrasern Chaihetphon
 *
 */
public class AppendToStringBuilder implements Runnable  {
	
	/** Declare character to append. */
	final char each = 'a';
	
	/** Declare counter that is number in loop . */
	int counter;

	/**
	 * Constructor of AppendToString.
	 * @param counter that is number of each that add.
	 */
	public AppendToStringBuilder(int counter){
		this.counter = counter;
	}
	
	/**
	 * To append char to String.
	 */
	public void run(){
		StringBuilder builder = new StringBuilder(); 
		int k = 0;
		while(k++ < counter) {
			builder = builder.append(each);
		}

		String result = builder.toString();
		System.out.println("final string length = " + result.length());
	}
	
	/**
	 * Get information about this class.
	 * @return String as What the class does.
	 */
	public String toString(){
		return String.format("Append to StringBuilder with count=%,d\n", counter);
	}

}
