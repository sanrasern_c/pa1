/**
 * This source code is Copyright 2015 by Sanrasern Chaihetphon.
 */
package stopwatch;

/**
 * To sum the value that from array.
 * @author Sanrasern Chaihetphon
 *
 */
public class SumDoublePrimitive implements Runnable {

	private double[] values;
	private int counter;
	private static final int ARRAY_SIZE = 500000;
	private double sum = 0.0;

	/**
	 * Constructor and initial value of attribute.
	 * @param counter as loop round.
	 */
	public SumDoublePrimitive(int counter){
		this.values = new double[ARRAY_SIZE];
		this.counter = counter;
	}
	
	/**
	 * Get information about this class.
	 * @return String as What the class does.
	 */
	public String toString(){
		return String.format("Sum array of double primitives with count=%,d\n", counter);
	}
	
	/**
	 * To run for execute varies code.
	 */
	public void run(){
		for(int k=0; k<ARRAY_SIZE; k++) values[k] = k+1;
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum + values[i];
		}
		System.out.println("sum = " + sum);
	}
	
}
