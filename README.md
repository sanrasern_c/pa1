#StopWatch By Sanrasern Chaihetphon (5710547247)#

I ran the tasks on a MacBook Pro, and got these
results:


```sh
 Task                                           |   Elapsed time (second)
 --------------------------------------------------------------------------
 TestAppendToString (length=100,000)            |      3.069499 second
 --------------------------------------------------------------------------
 TestAppendToStringBuilder (length=100,000)     |      0.002660 second
 --------------------------------------------------------------------------
 TestSumDoublePrimitive (count=100,000,000)     |      0.159225 second
 --------------------------------------------------------------------------
 TestSumDouble (count=100,000,000)              |      0.142495 second
 --------------------------------------------------------------------------
 TestSumBigDecimal (count=100,000,000)          |      0.775641 second
 --------------------------------------------------------------------------
```
###why is there such a big difference in the time used to append chars to a String and to a StringBuilder?###

Because when String add something to , they create a new object to collect it ,but StringBuilder use same object to store string.


###why is there a significant difference in times to sum double, Double, and BigDecimal values?###

Double and BigDecimal is object. When you want to use the real value java has to unboxing Double before get the value(double) to use .That is their are slow than primitive type.